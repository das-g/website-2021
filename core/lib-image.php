<?php

# Lib-image.php
# -------------
# Creates a _img($src_image, $alt_text, $thumb_width, $thumb_height, $jpg_quality)
# function to ease image resizing and caching 'on the fly'.
# Require a 'cache/' folder at root of website with write permissions.
# Usage: _img(picture.jpg, 'a sunny landscape', 640, 480, 85);
#
# @author: David Revoy
# @license: http://www.gnu.org/licenses/gpl.html GPL version 3 or higher
# Class inspired from code of PluXML, file: core/lib/class.plx.utils.php
# by Florent MONTHEL et Stephane F and the PluXML contributors.
# src: https://github.com/pluxml/PluXml/


# Check if we can write in cache/ before starting.
if (!is_writable($cache.'/')) {
  echo '<b style="color: red;font-family:monospace;">Carrot ask write permission to "'.$cache.'/"</b>';
}

# This function display the thumbnail if it exist, and if not create it and cache.
function _img($src_image, $alt_text, $thumb_width = 48, $thumb_height = 48, $jpg_quality = 90) {
  global $root;
  global $cache;
  # convert URL path to file (on disk) path
  $image_file = str_replace($root.'/', '', $src_image);
  if (file_exists($image_file)) {
    $image_filesize = filesize($image_file);
    $src_image_noextension = basename($src_image, '.jpg');
    $dest_image = $cache.'/'.$src_image_noextension.'_'.$thumb_width.'x'.$thumb_height.'px_'.$jpg_quality.'q_'.$image_filesize.'.jpg';
    # If thumbnail is not existing in cache
    if (!file_exists($dest_image)) {
      # Rebuild it
      _makeThumb($src_image, $dest_image, $thumb_width, $thumb_height, $jpg_quality);
      chmod($dest_image, 0775);
    }
    # Display
    echo '<img src="'.$root.'/'.$dest_image.'" alt="'.$alt_text.'" title="'.$alt_text.'">';
  } else {
    # Fallback in case of fail
    echo '<img src="'.$src_image.'" alt="404!" title="Bug: image is missing, please repair">';
  }
}

# This function create the thumbnail file.
function _makeThumb($src_image, $dest_image, $thumb_width = 48, $thumb_height = 48, $jpg_quality = 90) {

  # Get dimensions of existing image
  $image = getimagesize($src_image);

  # Check for valid dimensions
  if($image[0] <= 0 || $image[1] <= 0) {
    echo 'Error: No valid dimension';
  }

  # Check necessity to do math for the ratio
  if($thumb_width!=$thumb_height) {
    # math for the ratio
    $x_offset = $y_offset = 0;
    $square_size_w = $image[0];
    $square_size_h = $image[1];
    $ratio_w = $thumb_width / $image[0];
    $ratio_h = $thumb_height / $image[1];
    if($thumb_width == 0)
      $thumb_width = $image[0] * $ratio_h;
    elseif($thumb_height == 0)
      $thumb_height = $image[1] * $ratio_w;
    elseif($ratio_w < $ratio_h AND $ratio_w < 1) {
      $thumb_width = intval($ratio_w * $image[0]);
      $thumb_height = intval($ratio_w * $image[1]);
    } elseif($ratio_h < 1) {
      $thumb_width = $ratio_h * $image[0];
      $thumb_height = $ratio_h * $image[1];
    } else {
      $thumb_width = $image[0];
      $thumb_height = $image[1];
    }
  }

  $canvas = imagecreatetruecolor($thumb_width, $thumb_height);

  # Import image
  if (strpos($src_image, '.gif') == true) {
    $image_data = imagecreatefromgif($src_image);
  } else {
    $image_data = imagecreatefromjpeg($src_image);
  }

  # Verify import
  if($image_data == false) {
    echo 'Error: no image data';
  }

  # Calculate measurements (square crop)
  if($thumb_width==$thumb_height) {
    if($image[0] > $image[1]) {
      # For landscape images
      $x_offset = ($image[0] - $image[1]) / 2;
      $y_offset = 0;
      $square_size_w = intval($square_size_h = $image[0] - ($x_offset * 2));
    } else {
      # For portrait and square images
      $x_offset = 0;
      $y_offset = ($image[1] - $image[0]) / 2;
      $square_size_w = intval($square_size_h = $image[1] - ($y_offset * 2));
    }
  }


  # Resize and crop
  if( imagecopyresampled(
    $canvas,
    $image_data,
    0,
    0,
    $x_offset,
    $y_offset,
    $thumb_width,
    $thumb_height,
    $square_size_w,
    $square_size_h
  )) {

    # Sharpen filter:
      $sharpenMatrix = array (
        array (-1,-1,-1),
        array (-1,30,-1),
        array (-1,-1,-1),
      );
      $divisor = array_sum(array_map('array_sum', $sharpenMatrix));
      $offset = 0;
      imageconvolution ($canvas, $sharpenMatrix, $divisor, $offset);

    # Save to jpg
    return (imagejpeg($canvas, $dest_image, $jpg_quality) AND is_file($dest_image));

  } else {
    return false;
  }

} 

?>
